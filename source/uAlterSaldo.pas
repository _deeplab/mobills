unit uAlterSaldo;

interface

uses
  System.SysUtils, System.Types, System.UITypes, System.Classes, System.Variants, 
  FMX.Types, FMX.Graphics, FMX.Controls, FMX.Forms, FMX.Dialogs, FMX.StdCtrls,
  uParent, FMX.Controls.Presentation, FMX.Layouts, FMX.Objects, FMX.Edit, FMX.DialogService;

type
  TfAlterSaldo = class(TfParent)
    Panel1: TPanel;
    Panel2: TPanel;
    Panel3: TPanel;
    Panel4: TPanel;
    Label1: TLabel;
    Panel6: TPanel;
    Label2: TLabel;
    Label3: TLabel;
    Edit2: TEdit;
    Panel7: TPanel;
    Panel8: TPanel;
    Rectangle1: TRectangle;
    Label7: TLabel;
    Panel9: TPanel;
    Rectangle2: TRectangle;
    Label6: TLabel;
    Panel10: TPanel;
    Label8: TLabel;
    Panel5: TPanel;
    Panel11: TPanel;
    Panel12: TPanel;
    RadioButton1: TRadioButton;
    RadioButton2: TRadioButton;
    Label4: TLabel;
    Label5: TLabel;
    VertScrollBox1: TVertScrollBox;
    procedure FormCreate(Sender: TObject);
    procedure Edit2Exit(Sender: TObject);
    procedure RadioButton1Change(Sender: TObject);
    procedure RadioButton2Change(Sender: TObject);
    procedure Label6Click(Sender: TObject);
  private
    { Private declarations }
    var saldo, valAlt : Double;
    procedure initVariables();
  public
    { Public declarations }
  end;

var
  fAlterSaldo: TfAlterSaldo;

implementation

{$R *.fmx}

uses uDB;
{$R *.NmXhdpiPh.fmx ANDROID}
{$R *.LgXhdpiPh.fmx ANDROID}

procedure TfAlterSaldo.Edit2Exit(Sender: TObject);
begin
  inherited;
  if Edit2.Text <> '' then
    begin
      valAlt := StrToFloat(Edit2.Text);
    end
    else
      valAlt := 0;
  if RadioButton1.IsChecked then
  begin
    Label5.Text:= 'R$' + FormatFloat('0.00', saldo + valAlt);
  end;
  if RadioButton2.IsChecked then
  begin
    Label5.Text:= 'R$' + FormatFloat('0.00', saldo - valAlt);
  end;
end;

procedure TfAlterSaldo.initVariables;
begin
  saldo := dDatabase.getSaldo;
  valAlt := 0;
  Label8.Text:= 'R$' + FormatFloat('0.00', saldo);
  Label5.Text:= 'R$' + FormatFloat('0.00', saldo);
  Edit2.Text := '';
end;

procedure TfAlterSaldo.FormCreate(Sender: TObject);
begin
  inherited;
  initVariables;
end;

procedure TfAlterSaldo.Label6Click(Sender: TObject);
var saldoRes : double;
    res : boolean;
begin
  inherited;
  saldoRes := StrToFloat(StringReplace(Label5.Text,'R$','',[rfReplaceAll]));
  if saldoRes < 0 then
    begin
      TDialogService.MessageDialog('Seu saldo ficar� negativo, tem certeza?',
      TMsgDlgType.mtConfirmation, [TMsgDlgBtn.mbYes,TMsgDlgBtn.mbNo],
      TMsgDlgBtn.mbNo, 0,
      procedure(const AResult: TModalResult)
        begin
          if(AResult = mrYes)then
            begin
              res := dDatabase.alterSaldo(saldoRes);
              if(res) then
                begin
                  Showmessage('Saldo alterado com sucesso!');
                  initVariables;
                end
              else
                ShowMessage('Ocorreu um problema ao alterar saldo');
            end;
        end);
    end
  else
    begin
      res := dDatabase.alterSaldo(saldoRes);
      if(res) then
        begin
          Showmessage('Saldo alterado com sucesso!');
          initVariables;
        end
      else
        ShowMessage('Ocorreu um problema ao alterar saldo');
    end;
end;

procedure TfAlterSaldo.RadioButton1Change(Sender: TObject);
begin
  inherited;
  if RadioButton1.IsChecked then
  begin
    Label5.Text:= 'R$' + FormatFloat('0.00', saldo + valAlt);
  end;
end;

procedure TfAlterSaldo.RadioButton2Change(Sender: TObject);
begin
  inherited;
  if RadioButton2.IsChecked then
  begin
    Label5.Text:= 'R$' + FormatFloat('0.00', saldo - valAlt);
  end;
end;

end.
