program mobills;

uses
  System.StartUpCopy,
  FMX.Forms,
  uMain in 'uMain.pas' {fMain},
  uDB in 'uDB.pas' {dDatabase: TDataModule},
  uParent in 'uParent.pas' {fParent},
  uLoadBills in 'uLoadBills.pas' {fLoadBills},
  uCadConta in 'uCadConta.pas' {fCadastroConta},
  uConfUser in 'uConfUser.pas' {fCadConf},
  uGraficoGastos in 'uGraficoGastos.pas' {fGraficoGastos},
  uAlterSaldo in 'uAlterSaldo.pas' {fAlterSaldo},
  uInicial in 'uInicial.pas' {fInicial},
  uSaldoInit in 'uSaldoInit.pas' {fSaldoInit},
  uConstants in 'uConstants.pas',
  uListaContas in 'uListaContas.pas' {fParent1},
  uListaContasRecadastro in 'uListaContasRecadastro.pas' {fContas},
  uComparativoGrafico in 'uComparativoGrafico.pas' {fComparativoGastos};

{$R *.res}

begin
  Application.Initialize;
  Application.CreateForm(TfMain, fMain);
  Application.Run;
end.
