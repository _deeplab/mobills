unit uListaContas;

interface

uses
  System.SysUtils, System.Types, System.UITypes, System.Classes, System.Variants,
  FMX.Types, FMX.Graphics, FMX.Controls, FMX.Forms, FMX.Dialogs, FMX.StdCtrls,
  uParent, FMX.Layouts, FMX.Controls.Presentation, FMX.Objects, DateUtils, FMX.DialogService;

type
  TfListaConta = class(TfParent)
    Panel1: TPanel;
    VertScrollBox1: TVertScrollBox;
    Panel2: TPanel;
    Panel3: TPanel;
    Label1: TLabel;
    ScrollBox1: TScrollBox;
    Panel6: TPanel;
    Panel9: TPanel;
    Rectangle2: TRectangle;
    Label6: TLabel;
    Panel7: TPanel;
    procedure FormCreate(Sender: TObject);
    procedure Label6MouseDown(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Single);
    procedure Label6MouseUp(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Single);
    procedure Label6Click(Sender: TObject);
  private
    { Private declarations }
    procedure labelClick(Sender : TObject);
    procedure buttonClick(Sender : TObject);
    function confirmDlg(const aMsg : String) : boolean;
    procedure showContas();
    procedure reload(parName : String);
  public
    { Public declarations }
  end;

var
  fListaConta: TfListaConta;

implementation

{$R *.fmx}

uses uDB, uConstants, uMain, uCadConta, uSaldoInit;
{$R *.NmXhdpiPh.fmx ANDROID}
{$R *.LgXhdpiPh.fmx ANDROID}

{ TfListaConta }

procedure TfListaConta.buttonClick(Sender: TObject);
var res : boolean;
begin
  TDialogService.MessageDialog('Deseja realmente excluir o registro?',
        TMsgDlgType.mtConfirmation, [TMsgDlgBtn.mbYes,TMsgDlgBtn.mbNo],
        TMsgDlgBtn.mbNo, 0,
        procedure(const AResult: TModalResult)
          begin
            if(AResult = mrYes)then
              begin
                res := dDatabase.deleteConta(StrToInt(StringReplace((sender as TButton).Name, 'btn_', '', [rfReplaceAll])));
                if(res) then
                  begin
                    Showmessage('Deletado com sucesso!');
                    reload('rct_'+StringReplace((sender as TButton).Name, 'btn_', '', [rfReplaceAll]));
                  end
                else
                  ShowMessage('Erro ao deletar!');
              end;
          end);
end;

function TfListaConta.confirmDlg(const aMsg: String): boolean;
begin
end;

procedure TfListaConta.FormCreate(Sender: TObject);
begin
  showContas();
end;

procedure TfListaConta.Label6Click(Sender: TObject);
begin
  inherited;
  if(dDatabase.getSaldoInicial <> 0) then
    fMain.loadChild(TfCadastroConta.ClassName)
  else
    begin
      TDialogService.MessageDialog('� necess�rio um saldo inicial para o cadastro de conta!',
            TMsgDlgType.mtWarning, [TMsgDlgBtn.mbOK],
            TMsgDlgBtn.mbOK, 0,
            procedure(const AResult: TModalResult)
              begin
                if(AResult = mrOk)then
                  begin
                    fMain.loadChild(TfSaldoInit.ClassName);
                  end;
              end);
    end;
end;

procedure TfListaConta.Label6MouseDown(Sender: TObject; Button: TMouseButton;
  Shift: TShiftState; X, Y: Single);
begin
  inherited;
  Rectangle2.Fill.Color := TAlphaColors.Darkgreen;
end;

procedure TfListaConta.Label6MouseUp(Sender: TObject; Button: TMouseButton;
  Shift: TShiftState; X, Y: Single);
begin
  inherited;
  Rectangle2.Fill.Color := TAlphaColors.Limegreen;
end;

procedure TfListaConta.labelClick(Sender: TObject);
begin
  fMain.cadContaEdit(dDatabase.getConta(StrToInt(StringReplace((sender as TLabel).Name, 'lblConta_', '', [rfReplaceAll]))));
end;

procedure TfListaConta.reload(parName : String);
var I : integer;
    obj : TFmxObject;
begin
  //clear
  for I := ScrollBox1.Content.ChildrenCount-1 downto 0 do
    begin
      if(ScrollBox1.Content.Children.Items[I].Name = parName) then
        ScrollBox1.Content.RemoveObject(ScrollBox1.Content.Children.Items[I]);
    end;
end;

procedure TfListaConta.showContas;
var contas : TArray<TConta>;
    rect : TRectangle;
    lbl : TLabel;
    btn : TButton;
    I: Integer;
const dias_vencimento : Integer = -5;
      color_normal : TAlphaColor = TAlphaColors.Lightblue;
      color_proximo : TAlphaColor = TAlphaColors.Antiquewhite;
      color_vencido : TAlphaColor = TAlphaColors.Red;
      color_paga : TAlphaColor = TAlphaColors.Darkseagreen;
begin
  contas := dDatabase.getContas;
  for I := 0 to Length(contas)-1 do
    begin
      rect := TRectangle.Create(ScrollBox1);
      rect.Name := 'rct_'+IntToStr(contas[I].ID_CONTA);
      rect.Visible := true;
      if contas[I].FL_PAGO = 1 then
        rect.Fill.Color := color_paga
      else if contas[I].DT_VENCIMENTO < Now then
        rect.Fill.Color := color_vencido
      else if ((contas[I].DT_VENCIMENTO >= Now) and (contas[I].DT_VENCIMENTO > incDay(Now,dias_vencimento))) then
        rect.Fill.Color := color_proximo
      else
        rect.Fill.Color := color_normal;
      rect.Opacity := 1.0;
      rect.Parent := ScrollBox1;
      rect.Align := TAlignLayout.Top;
      rect.Stroke.Kind := TBrushKind.None;
      lbl := TLabel.Create(rect);
      lbl.Name := 'lblConta_'+IntToStr(contas[I].ID_CONTA);
      lbl.Align := TAlignLayout.Client;
      lbl.Text := '-> '+contas[I].DS_CONTA+' ('+DateToStr(contas[I].DT_VENCIMENTO)+')';
      lbl.TextSettings.Font.Style := lbl.TextSettings.Font.Style - [TFontStyle.fsBold];
      lbl.Visible := true;
      lbl.Parent := rect;
      lbl.HitTest := true;
      lbl.OnClick := labelClick;
      btn := TButton.Create(rect);
      btn.Name := 'btn_'+IntToStr(contas[I].ID_CONTA);
      btn.Visible := true;
      btn.Parent := rect;
      btn.Align := TAlignLayout.Right;
      btn.StyleLookup := 'deletetoolbutton';
      btn.Width := 50;
      btn.OnClick := buttonClick;
    end;
end;

end.
