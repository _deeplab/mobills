unit uConfUser;

interface

uses
  System.SysUtils, System.Types, System.UITypes, System.Classes, System.Variants, 
  FMX.Types, FMX.Graphics, FMX.Controls, FMX.Forms, FMX.Dialogs, FMX.StdCtrls,
  uParent, FMX.Edit, FMX.Objects, FMX.Controls.Presentation, FMX.Layouts;

type
  TfCadConf = class(TfParent)
    Panel1: TPanel;
    Panel2: TPanel;
    Panel3: TPanel;
    Panel4: TPanel;
    Panel5: TPanel;
    Label1: TLabel;
    Panel7: TPanel;
    Panel8: TPanel;
    Rectangle1: TRectangle;
    Label7: TLabel;
    Panel9: TPanel;
    Rectangle2: TRectangle;
    Label6: TLabel;
    Panel10: TPanel;
    Panel6: TPanel;
    Edit1: TEdit;
    Label2: TLabel;
    Edit2: TEdit;
    Label3: TLabel;
    VertScrollBox1: TVertScrollBox;
    procedure Edit1Exit(Sender: TObject);
    procedure Edit2Exit(Sender: TObject);
    procedure Label6Click(Sender: TObject);
    procedure FormCreate(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  fCadConf: TfCadConf;

implementation

{$R *.fmx}
{$R *.LgXhdpiPh.fmx ANDROID}
{$R *.NmXhdpiPh.fmx ANDROID}

uses uDB, uInicial, uMain;


procedure TfCadConf.Edit1Exit(Sender: TObject);
begin
  inherited;
  if(StrToInt(Edit1.Text) <= 0) or (StrToInt(Edit1.Text) > 15) then
    begin
      ShowMessage('dias de antecedencia para aviso do vencimento: <= 0 || > 15');
      Edit1.Text := '';
      Edit1.SetFocus;
    end;
end;

procedure TfCadConf.Edit2Exit(Sender: TObject);
begin
  inherited;
  if(StrToInt(Edit2.Text) <= 0) or (StrToInt(Edit2.Text) > 31) then
    begin
      showmessage('dia do aviso de recadastro: <= 0 || > 31');
      Edit2.Text := '';
      Edit2.SetFocus;
    end;
end;

procedure TfCadConf.FormCreate(Sender: TObject);
var a : TArray<Integer>;
begin
  inherited;
  a := dDatabase.getPreferenciasUsuario;
  if(a <> nil) then
    begin
      Edit1.Text := IntToStr(a[0]);
      Edit2.Text := IntToStr(a[1]);
    end;
end;

procedure TfCadConf.Label6Click(Sender: TObject);
var r : boolean;
begin
  inherited;
  r := dDatabase.salvarConfUser(StrToInt(Edit1.Text), StrToInt(Edit2.Text));
  if(r = true) then
    showmessage('salvo')
  else
    showmessage('erro');
end;

end.
