unit uConstants;

interface
  type TConta = record
    ID_CONTA : Integer;
    DS_CONTA : String;
    VL_CONTA : Double;
    DT_VENCIMENTO : TDate;
    FL_AVISAR_VENCIMENTO : Integer;
    TP_CONTA : Integer;
    FL_PAGO : Integer;
  end;

  type TValorGrafico = record
    TP_CONTA : Integer;
    QT_CONTA : Integer;
  end;

  type TContaRecad = record
    ID_CONTA : Integer;
    DS_CONTA : String;
    VL_CONTA : Double;
    DT_VENCIMENTO : TDate;
    FL_AVISAR_VENCIMENTO : Integer;
    TP_CONTA : Integer;
    FL_PAGO : Integer;
    FL_MARKED : boolean;
  end;

  type TComparativo =  record
    NR_MES : Integer;
    VL_CONTAS : Double;
  end;
implementation

end.
