object dDatabase: TdDatabase
  OldCreateOrder = False
  Height = 545
  Width = 464
  object Connection: TFDConnection
    Params.Strings = (
      'DriverID=SQLite')
    LoginPrompt = False
    AfterConnect = ConnectionAfterConnect
    BeforeConnect = ConnectionBeforeConnect
    Left = 56
    Top = 88
  end
  object FDPhysSQLiteDriverLink1: TFDPhysSQLiteDriverLink
    Left = 56
    Top = 8
  end
  object FDGUIxWaitCursor1: TFDGUIxWaitCursor
    Provider = 'FMX'
    Left = 184
    Top = 8
  end
  object Query: TFDQuery
    Connection = Connection
    Left = 184
    Top = 96
  end
  object EMSProvider1: TEMSProvider
    AndroidPush.GCMAppID = '658729146267'
    ApiVersion = '1'
    URLPort = 0
    Left = 80
    Top = 376
  end
  object PushEvents1: TPushEvents
    Provider = EMSProvider1
    AutoActivate = False
    AutoRegisterDevice = False
    Left = 160
    Top = 376
  end
end
