unit uCadConta;

interface

uses
  System.SysUtils, System.Types, System.UITypes, System.Classes, System.Variants,
  FMX.Types, FMX.Graphics, FMX.Controls, FMX.Forms, FMX.Dialogs, FMX.StdCtrls,
  uParent, FMX.DateTimeCtrls, FMX.ListBox, FMX.Edit, FMX.Controls.Presentation,
  FMX.Layouts, FMX.Objects, FMX.DialogService, uConstants;

type
  TfCadastroConta = class(TfParent)
    Panel1: TPanel;
    Panel2: TPanel;
    Panel3: TPanel;
    Panel4: TPanel;
    Panel5: TPanel;
    Label1: TLabel;
    Panel6: TPanel;
    Label2: TLabel;
    Edit1: TEdit;
    Label3: TLabel;
    Edit2: TEdit;
    Label4: TLabel;
    ComboBox1: TComboBox;
    Label5: TLabel;
    DateEdit1: TDateEdit;
    CheckBox1: TCheckBox;
    Panel7: TPanel;
    Panel8: TPanel;
    Panel9: TPanel;
    Rectangle1: TRectangle;
    Rectangle2: TRectangle;
    Label6: TLabel;
    Label7: TLabel;
    Panel10: TPanel;
    VertScrollBox1: TVertScrollBox;
    CheckBox2: TCheckBox;
    procedure FormCreate(Sender: TObject);
    procedure Label6Click(Sender: TObject);
    procedure Edit2Exit(Sender: TObject);
    procedure Label7Click(Sender: TObject);
    procedure Label6MouseDown(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Single);
    procedure Label7MouseDown(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Single);
    procedure Label7MouseUp(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Single);
    procedure Label6MouseUp(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Single);
  private
    { Private declarations }
    procedure ErrorDlg(const AMessage : String);
    var edit : boolean;
        conta_id : Integer;
  public
    { Public declarations }
    procedure loadConta(c : TConta);
  end;

var
  fCadastroConta: TfCadastroConta;

implementation

{$R *.fmx}
{$R *.LgXhdpiPh.fmx ANDROID}
{$R *.NmXhdpiPh.fmx ANDROID}

uses uDB, uMain, uInicial, uListaContas;


procedure TfCadastroConta.Edit2Exit(Sender: TObject);
var d : Double;
begin
  inherited;
  try
    d := StrToFloat(Edit2.Text);
    Edit2.Text := FormatFloat(',0.00',d);
  except
    Edit2.Text := '0,00';
  end;
end;

procedure TfCadastroConta.ErrorDlg(const AMessage: String);
begin
  TDialogService.MessageDialog(AMessage, TMsgDlgType.mtError, [TMsgDlgBtn.mbok],
                     TMsgDlgBtn.mbOk, 0, procedure(const AResult: TModalResult) begin end);
end;

procedure TfCadastroConta.FormCreate(Sender: TObject);
var
  I: Integer;
begin
  inherited;
  for I := 0 to ComboBox1.Count-1 do
    begin
      ComboBox1.ListItems[I].TextSettings.Font.Size := 20;
      ComboBox1.ListItems[I].StyledSettings :=  ComboBox1.ListItems[I].StyledSettings - [FMX.Types.TStyledSetting.Size];
    end;
  DateEdit1.Date := Now;
  edit := false;
end;

procedure TfCadastroConta.Label6Click(Sender: TObject);
var avisarVenc, contaPg : integer;
    r, res : boolean;
    saldo : double;
begin
  inherited;
  if(Edit1.Text = '') then
    begin
      ErrorDlg('O t�tulo da conta n�o pode ser vazio!');
      Exit;
    end;
  if(StrToFloatDef(Edit2.Text,0.0)= 0.0) then
    begin
      ErrorDlg('O valor da conta n�o pode ser vazio!');
      Exit;
    end;
  if(ComboBox1.ItemIndex = 0) then
    begin
      ErrorDlg('O tipo da conta n�o pode ser vazio!');
      Exit;
    end;
  if(CheckBox1.IsChecked)then
    avisarVenc := 1
  else
    avisarVenc := 0;
  if(CheckBox2.IsChecked)then
    begin
      contaPg := 1;
      TDialogService.MessageDialog('Descontar o pagamento do saldo?',
              TMsgDlgType.mtConfirmation, [TMsgDlgBtn.mbYes,TMsgDlgBtn.mbNo],
              TMsgDlgBtn.mbNo, 0,
              procedure(const AResult: TModalResult)
                begin
                  if(AResult = mrYes)then
                    begin
                      if(dDatabase.getSaldoInicial = 0) then
                        dDatabase.setSaldoInicial(StrToFloat(Edit2.Text)*-1);
                      saldo := dDatabase.getSaldo;
                      saldo := saldo - StrToFloat(Edit2.Text);
                      res := dDatabase.alterSaldo(saldo);
                      if(res) then
                        begin
                          Showmessage('Saldo alterado!');
                        end
                      else
                        ShowMessage('Erro ao alterar saldo, por favor verifique');
                    end;
                end);
    end
  else
    contaPg := 0;
  if(edit) then
    r := dDatabase.updateConta(conta_id,Edit1.Text,StrToFloat(Edit2.Text),DateEdit1.Date,avisarVenc,ComboBox1.ItemIndex,contaPg)
  else
    r := dDatabase.salvarConta(Edit1.Text,StrToFloat(Edit2.Text),DateEdit1.Date,avisarVenc,ComboBox1.ItemIndex, contaPg);

  if(r = true) then
    begin
      if(edit) then
        fMain.loadChild(TfInicial.ClassName)
      else
        begin
          fMain.loadChild(TfListaConta.ClassName);
        end;
    end
  else
    showmessage('N�o foi poss�vel salvar a conta!');
end;
procedure TfCadastroConta.Label6MouseDown(Sender: TObject; Button: TMouseButton;
  Shift: TShiftState; X, Y: Single);
begin
  inherited;
  Rectangle2.Fill.Color := TAlphaColors.Green;
end;

procedure TfCadastroConta.Label6MouseUp(Sender: TObject; Button: TMouseButton;
  Shift: TShiftState; X, Y: Single);
begin
  inherited;
  Rectangle2.Fill.Color := TAlphaColors.Limegreen;
end;

procedure TfCadastroConta.Label7Click(Sender: TObject);
begin
  inherited;
  if(edit) then
    fMain.loadChild(TfInicial.ClassName)
  else
    begin
      fMain.loadChild(TfListaConta.ClassName);
    end;
end;

procedure TfCadastroConta.Label7MouseDown(Sender: TObject; Button: TMouseButton;
  Shift: TShiftState; X, Y: Single);
begin
  inherited;
  Rectangle1.Fill.Color := TAlphaColors.Maroon;
end;

procedure TfCadastroConta.Label7MouseUp(Sender: TObject; Button: TMouseButton;
  Shift: TShiftState; X, Y: Single);
begin
  inherited;
  Rectangle1.Fill.Color := TAlphaColors.Darkred;
end;

procedure TfCadastroConta.loadConta(c: TConta);
begin
  conta_id := c.ID_CONTA;
  Edit1.Text := c.DS_CONTA;
  Edit2.Text := FormatFloat(',0.00',c.VL_CONTA);
  DateEdit1.Date := c.DT_VENCIMENTO;
  if(c.FL_AVISAR_VENCIMENTO = 0) then
    CheckBox1.IsChecked := false
  else
    CheckBox1.IsChecked := true;
  ComboBox1.ItemIndex := c.TP_CONTA;

  edit := true;
end;

end.
