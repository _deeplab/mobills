unit uLoadBills;

interface

uses
  System.SysUtils, System.Types, System.UITypes, System.Classes, System.Variants,
  FMX.Types, FMX.Graphics, FMX.Controls, FMX.Forms, FMX.Dialogs, FMX.StdCtrls, FMX.DialogService,
  uParent, FMX.Layouts, FMX.Controls.Presentation, uConstants, System.Threading;

type
  TfLoadBills = class(TfParent)
    Panel1: TPanel;
    Label1: TLabel;
    Panel2: TPanel;
    aiLoad: TAniIndicator;
  private
    { Private declarations }
  public
    { Public declarations }
    procedure activate(contas : TArray<TContaRecad>);
    procedure stop();
  end;

var
  fLoadBills: TfLoadBills;

implementation

{$R *.fmx}

uses uMain, uInicial;

{ TfLoadBills }

procedure TfLoadBills.activate(contas: TArray<TContaRecad>);
var contasR : TArray<TContaRecad>;
    ended, sEnd : Boolean;
begin
  sEnd := false;
  ended := false;
  contasR := contas;
  fMain.statsMenu(false);
  aiLoad.Visible := true;
  aiLoad.Enabled := true;
  TTask.run(
   procedure()
    begin
      Sleep(5000);
      stop;
    end
  );
end;

procedure TfLoadBills.stop;
begin
  fMain.statsMenu(true);
  TDialogService.MessageDialog('Contas recadastradas com sucesso!',
    TMsgDlgType.mtInformation, [TMsgDlgBtn.mbOK],
    TMsgDlgBtn.mbOK, 0,
    procedure(const AResult: TModalResult)
      begin
        if(AResult = mrOk)then
          begin
            //DO-Nothing
          end;
      end);
  fMain.loadChild(TfInicial.ClassName);
end;

end.
