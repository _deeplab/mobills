unit uDB;

interface

uses
  System.SysUtils, System.Classes, FireDAC.Stan.Intf, FireDAC.Stan.Option,
  FireDAC.Stan.Error, FireDAC.UI.Intf, FireDAC.Phys.Intf, FireDAC.Stan.Def,
  FireDAC.Stan.Pool, FireDAC.Stan.Async, FireDAC.Phys, FireDAC.Stan.ExprFuncs,
  FireDAC.Phys.SQLiteDef, FireDAC.FMXUI.Wait, FireDAC.Comp.UI, IOUtils,
  FireDAC.Phys.SQLite, Data.DB, FireDAC.Comp.Client, FireDAC.Stan.Param, uConstants,
  FireDAC.DatS, FireDAC.DApt.Intf, FireDAC.DApt, FireDAC.Comp.DataSet, FMX.Dialogs,
  REST.Backend.PushTypes, System.JSON, REST.Backend.EMSPushDevice, System.DateUtils,
  System.PushNotification, Data.Bind.Components, Data.Bind.ObjectScope, System.Threading,
  REST.Backend.BindSource, REST.Backend.PushDevice, REST.Backend.EMSProvider, System.Android.Service;

type
  TdDatabase = class(TDataModule)
    Connection: TFDConnection;
    FDPhysSQLiteDriverLink1: TFDPhysSQLiteDriverLink;
    FDGUIxWaitCursor1: TFDGUIxWaitCursor;
    Query: TFDQuery;
    EMSProvider1: TEMSProvider;
    PushEvents1: TPushEvents;
    procedure ConnectionBeforeConnect(Sender: TObject);
    procedure ConnectionAfterConnect(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
    //push
    var
    PushService : TPushService;
    ServiceConnection : TPushServiceConnection;
    procedure DoServiceReceiveNotification(Sender : TObject; const ServiceNotification : TPushServiceNotification);
    procedure DoServiceConnectionChanged(Sender : TObject; PushChanges : TPushService.TChanges);
    procedure DoPushOn();
    //contas
    function salvarConta(ds_conta : String; vl_conta : Double; dt_vencimento : TDate; fl_avisar_vencimento : Integer; tp_conta : Integer; fl_pago: Integer) : boolean;
    function deleteConta(id_conta : Integer) : boolean;
    function updateConta(id_conta : Integer; ds_conta : String; vl_conta : Double; dt_vencimento : TDate; fl_avisar_vencimento : Integer; tp_conta : Integer; fl_pago : Integer) : boolean;
    function getConta(id_conta : Integer) : TConta;
    function getValorPago() : Double;
    function getTotalContas() : Double;
    function getContasRecadastro() : TArray<TContaRecad>;
    //user
    function salvarConfUser(nr_dias_vencimento_aviso : Integer; nr_dia_aviso_recadastro : Integer) : boolean;
    function getDadosGraficoGastos() : TArray<TValorGrafico>;
    function getDadosComparativo() : TArray<TComparativo>;
    function getPreferenciasUsuario() : TArray<Integer>;
    function getContas() : TArray<TConta>;
    function alterSaldo(vl_saldo: Double) : boolean;
    function getSaldo() : Double;
    function setSaldoInicial(vl_saldo : Double) : boolean;
    function getSaldoInicial() : Double;
    procedure atualizaValorGastoMensal(nr_mes : Integer; vl_alteracao : Double; fl_alteracao: Integer);
  end;

var
  dDatabase: TdDatabase;

implementation

{%CLASSGROUP 'FMX.Controls.TControl'}

{$R *.dfm}

procedure TdDatabase.atualizaValorGastoMensal(nr_mes: Integer;
  vl_alteracao: Double; fl_alteracao: Integer);
var oper : String;
begin
  Query.Close;
  if(fl_alteracao = 0) then
    begin
      Query.SQL.Text := 'SELECT * FROM GASTO_MES WHERE NR_MES = :MES';
      Query.Params.ParamByName('MES').AsInteger := nr_mes;
      Query.Open;
      if(Query.IsEmpty) then
        oper := 'INSERT INTO GASTO_MES(NR_MES, VL_CONTAS_MES) VALUES (:NR_MES, :VL_CONTAS_MES)'
      else
        oper := 'UPDATE GASTO_MES SET VL_CONTAS_MES = (VL_CONTAS_MES + :VL_CONTAS_MES) WHERE NR_MES = :NR_MES';
      Query.Close;
      Query.SQL.Text := oper;
    end
  else
    begin
      Query.SQL.Text := 'UPDATE GASTO_MES SET VL_CONTAS_MES = (VL_CONTAS_MES - :VL_CONTAS_MES) WHERE NR_MES = :NR_MES';
    end;
  Query.Params.ParamByName('NR_MES').AsInteger := nr_mes;
  Query.Params.ParamByName('VL_CONTAS_MES').AsFloat := vl_alteracao;
  try
    Query.ExecSQL;
  except
    Connection.Rollback;
  end;
end;

procedure TdDatabase.ConnectionAfterConnect(Sender: TObject);
begin
  {$IFDEF DEBUG}
    //Connection.ExecSQL('DROP TABLE CONTA');
    //Connection.ExecSQL('DROP TABLE USER');
    //Connection.ExecSQL('DROP TABLE GASTO_MES');
  {$ENDIF}
  Connection.ExecSQL('CREATE TABLE IF NOT EXISTS CONTA('+
                                     'DS_CONTA TEXT NOT NULL, '+
                                     'VL_CONTA REAL NOT NULL, '+
                                     'DT_VENCIMENTO TEXT NOT NULL, '+
                                     'FL_AVISAR_VENCIMENTO INTEGER NOT NULL, '+
                                     'TP_CONTA INTEGER NOT NULL, '+
                                     'FL_PAGO INTEGER NOT NULL)');

  Connection.ExecSQL('CREATE TABLE IF NOT EXISTS USER('+
                                     'NR_DIAS_AVISO_VENCIMENTO INTEGER, '+
                                     'NR_DIA_AVISO_RECADASTRO_CONTAS INTEGER, '+
                                     'VL_SALDO REAL,'+
                                     'VL_SALDO_INICIAL REAL,'+
                                     'DS_TOKEN TEXT,'+
                                     'DT_LAST_NOTIFY_BILL TEXT)');

  Connection.ExecSQL('CREATE TABLE IF NOT EXISTS GASTO_MES('+
                                      'NR_MES INTEGER NOT NULL,'+
                                      'VL_CONTAS_MES REAL NOT NULL)');
end;

procedure TdDatabase.ConnectionBeforeConnect(Sender: TObject);
begin
  {$IF DEFINED(IOS) or DEFINED(ANDROID)}
    Connection.Params.Values['Database'] := TPath.GetDocumentsPath + PathDelim + 'mobills.s3db';
  {$ELSE}
    Connection.Params.Values['Database'] := ExtractFilePath(ParamStr(0))+'mobills.s3db';
  {$ENDIF}
end;

function TdDatabase.deleteConta(id_conta: Integer): boolean;
var
  conta : TConta;
begin
  conta := getConta(id_conta);

  Query.Close;
  Query.SQL.Text := 'DELETE FROM CONTA WHERE rowid = :ROWID';
  Query.Params.ParamByName('ROWID').AsInteger := id_conta;

  try
    Query.ExecSQL;
    atualizaValorGastoMensal(MonthOf(conta.DT_VENCIMENTO),conta.VL_CONTA,1);
    Result := true;
  except
    Connection.Rollback;
    Result := false;
  end;
end;

procedure TdDatabase.DoPushOn;
begin
  {$IFDEF IOS}
    PushService := TPushServiceManager.Instance.GetServiceByName(TPushService.TServiceNames.APS);
    ServiceConnection := TPushServiceConnection.Create(PushService);
    ServiceConnection.onChange := DoServiceConnectionChanged;
    ServiceConnection.onReceiveNotification := DoServiceReceiveNotification;

    TTask.run(procedure
    begin
      ServiceConnection.Active := true;
    end);
  {$ELSE}
    PushService := TPushServiceManager.Instance.GetServiceByName(TPushService.TServiceNames.GCM);
    PushService.AppProps[TPushService.TAppPropNames.GCMAppID] := '943284183875';
    ServiceConnection := TPushServiceConnection.Create(PushService);
    ServiceConnection.onChange := DoServiceConnectionChanged;
    ServiceConnection.onReceiveNotification := DoServiceReceiveNotification;

    TTask.run(procedure
    begin
      ServiceConnection.Active := true;
    end);
  {$ENDIF}
end;

procedure TdDatabase.DoServiceConnectionChanged(Sender: TObject;
  PushChanges: TPushService.TChanges);
var oper : String;
    token, lastToken : String;
    FServiceConnection : TLocalServiceConnection;
begin
  lastToken := '';
  token := '';
  if TPushService.TChange.DeviceToken in PushChanges then
    begin
      Query.Close;
      Query.SQL.Text := 'SELECT * FROM USER';
      Query.Open;
      if Query.IsEmpty then
        begin
          Query.Close;
          Query.SQL.Text := 'INSERT INTO USER(DT_LAST_NOTIFY_BILL) VALUES(:DT)';
          Query.Params.ParamByName('DT').AsString := DateToStr(Now);
          Query.ExecSQL();
        end
      else
        begin
          if Query.FieldByName('DT_LAST_NOTIFY_BILL').AsString = '' then
            begin
              Query.Close;
              Query.SQL.Text := 'UPDATE USER SET DT_LAST_NOTIFY_BILL = :DT';
              Query.Params.ParamByName('DT').AsString := DateToStr(Now);
              Query.ExecSQL();
            end;
        end;
      Query.Close;
      token := PushService.DeviceTokenValue[TPushService.TDeviceTokenNames.DeviceToken];

      if token <> '' then
        begin
          Query.Close;
          Query.SQL.Text := 'SELECT DS_TOKEN FROM USER';
          Query.Open;
          if Query.IsEmpty then
              oper := 'INSERT INTO USER(DS_TOKEN)'+
              'VALUES(:DS_TOKEN)'
          else
            begin
              lastToken := Query.FieldByName('DS_TOKEN').AsString;
              oper := 'UPDATE USER SET DS_TOKEN = :DS_TOKEN';
            end;

          if token <> lastToken then
            begin
              Query.Close;
              Query.SQL.Text := oper;
              Query.Params.ParamByName('DS_TOKEN').AsString := token;

              try
                Query.ExecSQL;
              except
                Connection.Rollback;
              end;
            end;
        end;
      FServiceConnection := TLocalServiceConnection.Create;
      FServiceConnection.StartService('servMobills');
    end;
end;

procedure TdDatabase.DoServiceReceiveNotification(Sender: TObject;
  const ServiceNotification: TPushServiceNotification);
var msg_text : String;
    x : integer;
begin
  for x := 0 to ServiceNotification.DataObject.Count - 1 do
    begin
      //iOS
      if(ServiceNotification.DataKey = 'aps') then
        begin
          if ServiceNotification.DataObject.Pairs[x].JsonString.Value = 'alert' then
            msg_text := ServiceNotification.DataObject.Pairs[x].JsonString.Value;
        end;

      //Android
      if(ServiceNotification.DataKey = 'gcm') then
        begin
          if ServiceNotification.DataObject.Pairs[x].JsonString.Value = 'message' then
            msg_text := ServiceNotification.DataObject.Pairs[x].JsonString.Value;
        end;
    end;
  ShowMessage(msg_text);
end;

function TdDatabase.getDadosGraficoGastos: TArray<TValorGrafico>;
var
  indx : Integer;
  tp_conta : TArray<Integer>;
begin
  Query.Close;
  Query.SQL.Text := 'select distinct tp_conta from conta where dt_vencimento like :dt_inicio_mes';
  Query.Params.ParamByName('DT_INICIO_MES').AsString := '%/'+MonthOf(Now)+'/'+YearOf(Now);
  Query.Open;
  Query.Last;
  if(Query.RecordCount > 0) then
    begin
      SetLength(Result,Query.RecordCount);
      SetLength(tp_conta,Query.RecordCount);
      Query.First;
      indx := 0;
      while not Query.Eof do
        begin
          tp_conta[indx] := Query.FieldByName('TP_CONTA').AsInteger;
          indx := indx +1;
          Query.Next
        end;

      for indx := 0 to Length(tp_conta)-1 do
        begin
          Query.Close;
          Query.SQL.Text := 'select count(*) qt_reg from conta where tp_conta = :tp_conta';
          Query.Params.ParamByName('TP_CONTA').AsInteger := tp_conta[indx];
          Query.Open;
          Result[indx].TP_CONTA:= tp_conta[indx];
          Result[indx].QT_CONTA:= Query.FieldByName('QT_REG').AsInteger;
        end;
    end
  else
    Result := nil;
end;

function TdDatabase.getDadosComparativo: TArray<TComparativo>;
var
  indx : Integer;
begin
  Query.Close;
  Query.SQL.Text := 'select * from GASTO_MES';
  Query.Open;
  Query.Last;
  if(Query.RecordCount > 0) then
    begin
      SetLength(Result,Query.RecordCount);
      Query.First;
      indx := 0;
      while not Query.Eof do
        begin
          Result[indx].NR_MES:= Query.FieldByName('NR_MES').AsInteger;
          Result[indx].VL_CONTAS:= Query.FieldByName('VL_CONTAS_MES').AsFloat;
          indx := indx +1;
          Query.Next
        end;
    end
  else
    Result := nil;
end;

function TdDatabase.getPreferenciasUsuario: TArray<Integer>;
begin
  Query.Close;
  Query.SQL.Text := 'SELECT * FROM USER';
  Query.Open;
  Query.Last;
  if(Query.RecordCount >= 1) then
    begin
      SetLength(Result,2);
      Query.Close;
      Query.SQL.Text := 'SELECT NR_DIAS_AVISO_VENCIMENTO, NR_DIA_AVISO_RECADASTRO_CONTAS FROM USER';
      Query.Open;

      Result[0] := Query.FieldByName('NR_DIAS_AVISO_VENCIMENTO').AsInteger;
      Result[1] := Query.FieldByName('NR_DIA_AVISO_RECADASTRO_CONTAS').AsInteger;
    end
  else
    Result := nil;
end;

function TdDatabase.getConta(id_conta: Integer): TConta;
var conta : TConta;
begin
  Query.Close;
  Query.SQL.Text := 'SELECT rowid, * FROM CONTA WHERE rowid = :rowid';
  Query.Params.ParamByName('rowid').AsInteger := id_conta;
  Query.Open;
  Query.Last;
  if(Query.RecordCount > 0) then
    begin
      Query.First;
      conta.ID_CONTA := Query.FieldByName('rowid').AsInteger;
      conta.DS_CONTA := Query.FieldByName('DS_CONTA').asString;
      conta.VL_CONTA := Query.FieldByName('VL_CONTA').AsFloat;
      conta.DT_VENCIMENTO := StrToDate(Query.FieldByName('DT_VENCIMENTO').AsString);
      conta.FL_AVISAR_VENCIMENTO := Query.FieldByName('FL_AVISAR_VENCIMENTO').AsInteger;
      conta.TP_CONTA := Query.FieldByName('TP_CONTA').AsInteger;
      conta.FL_PAGO := Query.FieldByName('FL_PAGO').AsInteger;
      Result := conta;
    end;
end;

function TdDatabase.getContas: TArray<TConta>;
var conta : TConta;
    idx : Integer;
begin
  idx := 0;
  Query.Close;
  Query.SQL.Text := 'SELECT rowid, * FROM CONTA';
  Query.Open;
  Query.Last;
  if(Query.RecordCount > 0) then
    begin
      SetLength(Result,Query.RecordCount);
      Query.First;
      while not Query.Eof do
        begin
          if(MonthOf(StrToDate(Query.FieldByName('DT_VENCIMENTO').AsString)) >= MonthOf(Now)) or
             (Query.FieldByName('FL_PAGO').AsInteger = 0) then
            begin
              conta.ID_CONTA := Query.FieldByName('rowid').AsInteger;
              conta.DS_CONTA := Query.FieldByName('DS_CONTA').asString;
              conta.VL_CONTA := Query.FieldByName('VL_CONTA').AsFloat;
              conta.DT_VENCIMENTO := StrToDate(Query.FieldByName('DT_VENCIMENTO').AsString);
              conta.FL_AVISAR_VENCIMENTO := Query.FieldByName('FL_AVISAR_VENCIMENTO').AsInteger;
              conta.TP_CONTA := Query.FieldByName('TP_CONTA').AsInteger;
              conta.FL_PAGO := Query.FieldByName('FL_PAGO').AsInteger;
              Result[idx] := conta;
              inc(idx);
            end
          else
            SetLength(Result,Length(Result)-1);
          Query.Next;
        end;
    end
  else
    Result := nil;
end;

function TdDatabase.getContasRecadastro: TArray<TContaRecad>;
var month : Integer;
    conta : TContaRecad;
    idx : Integer;
begin
  idx := 0;
  Query.Close;
  Query.SQL.Text := 'SELECT rowid, * FROM CONTA';
  Query.Open;
  Query.Last;
  if(Query.RecordCount > 0) then
    begin
      SetLength(Result,Query.RecordCount);
      Query.First;
      while not Query.Eof do
        begin
          if(MonthOf(StrToDate(Query.FieldByName('DT_VENCIMENTO').AsString)) < MonthOf(Now)) then
            begin
              conta.ID_CONTA := Query.FieldByName('rowid').AsInteger;
              conta.DS_CONTA := Query.FieldByName('DS_CONTA').asString;
              conta.VL_CONTA := Query.FieldByName('VL_CONTA').AsFloat;
              conta.DT_VENCIMENTO := StrToDate(Query.FieldByName('DT_VENCIMENTO').AsString);
              conta.FL_AVISAR_VENCIMENTO := Query.FieldByName('FL_AVISAR_VENCIMENTO').AsInteger;
              conta.TP_CONTA := Query.FieldByName('TP_CONTA').AsInteger;
              conta.FL_PAGO := Query.FieldByName('FL_PAGO').AsInteger;
              conta.FL_MARKED := false;
              Result[idx] := conta;
              inc(idx);
            end
          else
            SetLength(Result,Length(Result)-1);
          Query.Next;
        end;
    end
  else
    Result := nil;
end;

function TdDatabase.salvarConfUser(nr_dias_vencimento_aviso, nr_dia_aviso_recadastro: Integer): boolean;
var oper : String;
begin
  Query.Close;
  Query.SQL.Text := 'SELECT * FROM USER';
  Query.Open;
  Query.Last;
  if(Query.RecordCount < 1) then
      oper := 'INSERT INTO USER(NR_DIAS_AVISO_VENCIMENTO, NR_DIA_AVISO_RECADASTRO_CONTAS)'+
      'VALUES(:NR_DIAS_AVISO_VENCIMENTO,:NR_DIA_AVISO_RECADASTRO_CONTAS)'
  else
    oper := 'UPDATE USER SET NR_DIAS_AVISO_VENCIMENTO = :NR_DIAS_AVISO_VENCIMENTO, '+
              'NR_DIA_AVISO_RECADASTRO_CONTAS = :NR_DIA_AVISO_RECADASTRO_CONTAS';


  Query.Close;
  Query.SQL.Text := oper;
  Query.Params.ParamByName('NR_DIAS_AVISO_VENCIMENTO').AsInteger := nr_dias_vencimento_aviso;
  Query.Params.ParamByName('NR_DIA_AVISO_RECADASTRO_CONTAS').AsInteger := nr_dia_aviso_recadastro;

  try
    Query.ExecSQL;
    Result := true;
  except
    Connection.Rollback;
    Result := false;
  end;
end;

function TdDatabase.updateConta(id_conta: Integer; ds_conta: String;
  vl_conta: Double; dt_vencimento: TDate; fl_avisar_vencimento,
  tp_conta: Integer; fl_pago: Integer): boolean;
var vl_antigo : Double;
begin
  Query.Close;
  Query.SQL.Text := 'SELECT VL_CONTA FROM CONTA WHERE rowid = :rowid';
  Query.Params.ParamByName('rowid').AsInteger := id_conta;
  Query.ExecSQL;
  vl_antigo := Query.FieldByName('VL_CONTA').AsFloat;

  Query.Close;
  Query.SQL.Text := 'UPDATE CONTA SET '+
                    'DS_CONTA = :DS_CONTA, '+
                    'VL_CONTA = :VL_CONTA, '+
                    'DT_VENCIMENTO = :DT_VENCIMENTO, '+
                    'FL_AVISAR_VENCIMENTO = :FL_AVISAR_VENCIMENTO, '+
                    'TP_CONTA = :TP_CONTA, '+
                    'FL_PAGO = :FL_PAGO '+
                    'WHERE rowid = :rowid';
  Query.Params.ParamByName('DS_CONTA').AsString := ds_conta;
  Query.Params.ParamByName('VL_CONTA').AsFloat := vl_conta;
  Query.Params.ParamByName('DT_VENCIMENTO').AsString := DateToStr(dt_vencimento);
  Query.Params.ParamByName('FL_AVISAR_VENCIMENTO').AsInteger := fl_avisar_vencimento;
  Query.Params.ParamByName('TP_CONTA').AsInteger := tp_conta;
  Query.Params.ParamByName('FL_PAGO').AsInteger := fl_pago;
  Query.Params.ParamByName('rowid').AsInteger := id_conta;

  try
    Query.ExecSQL;
    if (vl_antigo > vl_conta) then
      atualizaValorGastoMensal(MonthOf(dt_vencimento),vl_antigo - vl_conta,1)
    else if(vl_conta > vl_antigo) then
      atualizaValorGastoMensal(MonthOf(dt_vencimento),vl_conta - vl_antigo,0);
    Result := true;
  except
    Connection.Rollback;
    Result := false;
  end;
end;

function TdDatabase.salvarConta(ds_conta: String; vl_conta: Double;
  dt_vencimento: TDate; fl_avisar_vencimento : Integer; tp_conta : Integer; fl_pago: Integer): boolean;
begin
  Query.Close;
  Query.SQL.Text := 'INSERT INTO CONTA(DS_CONTA,VL_CONTA,DT_VENCIMENTO,'+
                    'FL_AVISAR_VENCIMENTO,TP_CONTA,FL_PAGO)VALUES('+
                    ':DS_CONTA,:VL_CONTA,:DT_VENCIMENTO,:FL_AVISAR_VENCIMENTO,:TP_CONTA, :FL_PAGO)';
  Query.Params.ParamByName('DS_CONTA').AsString := ds_conta;
  Query.Params.ParamByName('VL_CONTA').AsFloat := vl_conta;
  Query.Params.ParamByName('DT_VENCIMENTO').AsString := DateToStr(dt_vencimento);
  Query.Params.ParamByName('FL_AVISAR_VENCIMENTO').AsInteger := fl_avisar_vencimento;
  Query.Params.ParamByName('TP_CONTA').AsInteger := tp_conta;
  Query.Params.ParamByName('FL_PAGO').AsInteger := fl_pago;

  try
    Query.ExecSQL;
    atualizaValorGastoMensal(MonthOf(dt_vencimento),vl_conta,0);
    Result := true;
  except
    Connection.Rollback;
    Result := false;
  end;
end;

function TdDatabase.setSaldoInicial(vl_saldo: Double): boolean;
var oper : String;
begin
  Query.Close;
  Query.SQL.Text := 'SELECT * FROM USER';
  Query.Open;
  Query.Last;
  if(Query.RecordCount < 1) then
    oper := 'INSERT INTO USER(VL_SALDO, VL_SALDO_INICIAL)'+
    'VALUES(:VL_SALDO, :VL_SALDO)'
  else
    oper := 'UPDATE USER SET VL_SALDO = :VL_SALDO, VL_SALDO_INICIAL = :VL_SALDO';

  Query.Close;
  Query.SQL.Text := oper;
  Query.Params.ParamByName('VL_SALDO').AsFloat := vl_saldo;

  try
    Query.ExecSQL;
    Result := true;
  except
    Connection.Rollback;
    Result := false;
  end;
end;

function TdDatabase.alterSaldo(vl_saldo: Double) : boolean;
var oper : String;
begin
  Query.Close;
  Query.SQL.Text := 'SELECT * FROM USER';
  Query.Open;
  Query.Last;
  if(Query.RecordCount < 1) then
      oper := 'INSERT INTO USER(VL_SALDO)'+
      'VALUES(:VL_SALDO)'
  else
    oper := 'UPDATE USER SET VL_SALDO = :VL_SALDO';


  Query.Close;
  Query.SQL.Text := oper;
  Query.Params.ParamByName('VL_SALDO').AsFloat := vl_saldo;

  try
    Query.ExecSQL;
    Result := true;
  except
    Connection.Rollback;
    Result := false;
  end;
end;

function TdDatabase.getSaldo: Double;
begin
  Query.Close;
  Query.SQL.Text := 'SELECT * FROM USER';
  Query.Open;
  Query.Last;
  if(Query.RecordCount >= 1) then
    begin
      Query.Close;
      Query.SQL.Text := 'SELECT VL_SALDO FROM USER';
      Query.Open;

      Result := Query.FieldByName('VL_SALDO').AsFloat;
    end
  else
    Result := 0;
end;

function TdDatabase.getSaldoInicial: Double;
begin
  Query.Close;
  Query.SQL.Text := 'SELECT * FROM USER';
  Query.Open;
  Query.Last;
  if(Query.RecordCount >= 1) then
    begin
      Query.Close;
      Query.SQL.Text := 'SELECT VL_SALDO_INICIAL FROM USER';
      Query.Open;

      Result := Query.FieldByName('VL_SALDO_INICIAL').AsFloat;
    end
  else
    Result := 0;
end;

function TdDatabase.getTotalContas: Double;
var conta : TArray<TConta>;
  I: Integer;
begin
  conta := getContas;
  Result := 0;
  for I := 0 to Length(conta)-1 do
    begin
      Result := Result + conta[I].VL_CONTA;
    end;
end;

function TdDatabase.getValorPago: Double;
var contas : TArray<TConta>;
    I : Integer;
begin
  contas := getContas;
  Result := 0;
  for I := 0 to Length(contas)-1 do
    begin
      if contas[I].FL_PAGO = 1 then
        Result := Result + contas[I].VL_CONTA;
    end;
end;

end.
