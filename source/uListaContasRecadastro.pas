unit uListaContasRecadastro;

interface

uses
  System.SysUtils, System.Types, System.UITypes, System.Classes, System.Variants,
  FMX.Types, FMX.Graphics, FMX.Controls, FMX.Forms, FMX.Dialogs, FMX.StdCtrls,
  uParent, FMX.Layouts, FMX.Controls.Presentation, uConstants, FMX.Objects, FMX.DialogService;

type
  TfContas = class(TfParent)
    VertScrollBox1: TVertScrollBox;
    Panel1: TPanel;
    Panel2: TPanel;
    Panel3: TPanel;
    Label1: TLabel;
    ScrollBox1: TScrollBox;
    Panel6: TPanel;
    Panel9: TPanel;
    Panel4: TPanel;
    Rectangle2: TRectangle;
    Label6: TLabel;
    Panel7: TPanel;
    procedure FormCreate(Sender: TObject);
    procedure Label6Click(Sender: TObject);
    procedure Label6MouseDown(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Single);
    procedure Label6MouseUp(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Single);
  private
    { Private declarations }
  public
    { Public declarations }
    procedure tcbClick(Sender : TObject);
  end;

var
  fContas: TfContas;
  conta : TArray<TContaRecad>;

implementation

{$R *.fmx}

uses uDB, uMain;

procedure TfContas.FormCreate(Sender: TObject);
var
    tcb : TCheckBox;
  I: Integer;
begin
  inherited;
  conta := dDatabase.getContasRecadastro;
  for I := 0 to Length(conta)-1 do
    begin
      tcb := TCheckBox.Create(ScrollBox1);
      tcb.Parent := ScrollBox1;
      tcb.Visible := true;
      tcb.Height := 30;
      tcb.Width := 50;
      tcb.Align := TAlignLayout.Top;
      tcb.Text := conta[I].DS_CONTA;
      tcb.TextSettings.Font.Size := 16;
      tcb.TextSettings.Font.Style := [TFontStyle.fsBold];
      tcb.StyledSettings := [FMX.Types.TStyledSetting.FontColor, FMX.Types.TStyledSetting.Family];
      tcb.Name := 'cbx_'+IntToStr(conta[I].ID_CONTA);
      tcb.OnClick := tcbClick;
    end;
end;

procedure TfContas.Label6Click(Sender: TObject);
var I : Integer;
    B : boolean;
begin
  inherited;
  B := false;
  for I := 0 to Length(conta)-1 do
    begin
      if conta[I].FL_MARKED then
        B := True;

      if B then
        break;
    end;
  if B then
    fMain.recadastroContas(conta)
  else
    TDialogService.MessageDialog('Nenhuma conta selecionada!',
        TMsgDlgType.mtWarning, [TMsgDlgBtn.mbOK],
        TMsgDlgBtn.mbOK, 0,
        procedure(const AResult: TModalResult)
          begin
            if(AResult = mrOk)then
              begin
                //DO-nothing
              end;
          end);
end;

procedure TfContas.Label6MouseDown(Sender: TObject; Button: TMouseButton;
  Shift: TShiftState; X, Y: Single);
begin
  inherited;
  Rectangle2.Fill.Color := TAlphaColors.Darkgreen;
end;

procedure TfContas.Label6MouseUp(Sender: TObject; Button: TMouseButton;
  Shift: TShiftState; X, Y: Single);
begin
  inherited;
  Rectangle2.Fill.Color := TAlphaColors.Limegreen;
end;

procedure TfContas.tcbClick(Sender: TObject);
var str : String;
  I: Integer;
begin
  str := StringReplace((Sender as TCheckbox).Name,'cbx_','',[rfReplaceAll]);
  for I := 0 to Length(conta)-1 do
    begin
      if(conta[I].ID_CONTA = StrToInt(str)) then
        begin
          conta[I].FL_MARKED := not conta[I].FL_MARKED;
          break;
        end;
    end;
end;

end.
