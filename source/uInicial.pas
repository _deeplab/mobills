unit uInicial;

interface

uses
  System.SysUtils, System.Types, System.UITypes, System.Classes, System.Variants,
  FMX.Types, FMX.Graphics, FMX.Controls, FMX.Forms, FMX.Dialogs, FMX.StdCtrls,
  uParent, FMX.Objects, FMX.Controls.Presentation, FMX.Layouts;

type
  TfInicial = class(TfParent)
    Panel1: TPanel;
    Panel2: TPanel;
    Panel3: TPanel;
    Panel4: TPanel;
    Panel5: TPanel;
    Panel6: TPanel;
    Label1: TLabel;
    TPAlterSaldo: TPanel;
    TPCadSaldo: TPanel;
    Label2: TLabel;
    Panel10: TPanel;
    Panel9: TPanel;
    Panel11: TPanel;
    Panel12: TPanel;
    Label3: TLabel;
    Rectangle2: TRectangle;
    Label6: TLabel;
    VertScrollBox1: TVertScrollBox;
    TPBarSaldo: TPanel;
    Panel14: TPanel;
    TPBarConta: TPanel;
    Label4: TLabel;
    Label5: TLabel;
    Panel16: TPanel;
    Panel17: TPanel;
    Panel18: TPanel;
    Rectangle1: TRectangle;
    Label7: TLabel;
    ctNaoPaga: TRectangle;
    ctPaga: TRectangle;
    sldGasto: TRectangle;
    sldAtual: TRectangle;
    procedure Label6Click(Sender: TObject);
    procedure Label7Click(Sender: TObject);
    procedure FormCreate(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  fInicial: TfInicial;

implementation

{$R *.fmx}

uses uMain, uDB, uConstants;
{$R *.NmXhdpiPh.fmx ANDROID}
{$R *.LgXhdpiPh.fmx ANDROID}

procedure TfInicial.FormCreate(Sender: TObject);
var saldoInicial, saldo : Double;
    perc : Single;
    contaPaga, contaTotal : Double;
    totalWidth : Single;
begin
  inherited;
  saldoInicial := dDatabase.getSaldoInicial;
  saldo := dDatabase.getSaldo;

  contaPaga := dDatabase.getValorPago;
  contaTotal := dDatabase.getTotalContas;

  totalWidth := Screen.Width - (Panel2.Width + Panel3.Width);

  if(saldoInicial = 0) then
    begin
      TPCadSaldo.Visible := true;
      TPAlterSaldo.Visible := false;
    end
  else
    begin
      TPCadSaldo.Visible := false;
      TPAlterSaldo.Visible := true;
    end;

  if(TPAlterSaldo.Visible) then
    begin
      if(saldo >= saldoInicial) then
        begin
          sldAtual.Width := totalWidth;
          sldGasto.Width := 0;
        end
      else if(saldo <= 0) then
        begin
          sldGasto.Width := totalWidth;
          sldAtual.Width := 0;
        end
      else
        begin
          //percentual gasto
          perc := Round((100 * saldo) / saldoInicial);
          //percentual da barra ocupada pelo gasto
          sldAtual.Width := Round((totalWidth * perc) / 100);
          //total de width
          sldGasto.Width := Round(totalWidth - sldAtual.Width);
        end;
    end;
  Label4.Text := 'Voc� possui R$ '+FormatFloat(',0.00',saldo)+' e come�ou o m�s com R$ '+FormatFloat(',0.00',saldoInicial);
    //to do altera��es da barra da conta
  perc := 0;
  if(contaPaga = contaTotal) then
    begin
      ctPaga.Width := totalWidth-1;
      ctNaoPaga.Width := 1;
    end
  else if(contaPaga <= 0) then
    begin
      ctNaoPaga.Width := totalWidth-1;
      ctPaga.Width := 1;
    end
  else
    begin
      //percentual pago
      perc := Round((100 * contaPaga) / contaTotal);
      //percentual da barra ocupada pelo pago
      ctPaga.Width := Round((totalWidth * perc) / 100);
      //total de width
      ctNaoPaga.Width := Round(totalWidth - ctPaga.Width);
    end;
  Label5.Text := 'R$ '+FormatFloat(',0.00',contaPaga)+' pagos de R$ '+FormatFloat(',0.00',contaTotal)+' cadastrados'
end;

procedure TfInicial.Label6Click(Sender: TObject);
begin
  inherited;
  fMain.saldoInitBtClick(Sender);
end;

procedure TfInicial.Label7Click(Sender: TObject);
begin
  inherited;
  fMain.saldoAltBtClick(Sender);
end;

end.
