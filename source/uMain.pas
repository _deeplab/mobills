unit uMain;

interface

uses
  System.SysUtils, System.Types, System.UITypes, System.Classes, System.Variants,
  FMX.Types, FMX.Controls, FMX.Forms, FMX.Graphics, FMX.Dialogs, FMX.StdCtrls,
  FMX.Controls.Presentation, FMX.Layouts, uParent, FMX.ListBox, FMX.Objects,
  FMX.MultiView, uConstants, System.Android.Service, REST.Backend.PushTypes,
  FMX.DialogService;

type
  TfMain = class(TForm)
    tbTop: TToolBar;
    btLateralMenu: TButton;
    Label1: TLabel;
    loMain: TLayout;
    lstPopUp: TListBox;
    ListBoxItem1: TListBoxItem;
    ListBoxItem2: TListBoxItem;
    ListBoxItem3: TListBoxItem;
    ListBoxItem5: TListBoxItem;
    ListBoxItem6: TListBoxItem;
    ListBoxItem4: TListBoxItem;
    ListBoxItem7: TListBoxItem;
    Layout1: TLayout;
    MultiView1: TMultiView;
    ToolBar2: TToolBar;
    Button2: TButton;
    ListBoxItem8: TListBoxItem;
    procedure FormCreate(Sender: TObject);
    procedure ListBoxItem1Click(Sender: TObject);
    procedure ListBoxItem2Click(Sender: TObject);
    procedure ListBoxItem3Click(Sender: TObject);
    procedure ListBoxItem6Click(Sender: TObject);
    procedure saldoInitBtClick(Sender: TObject);
    //n�o mexe
    procedure loadChild(AClass : String);
    procedure Button2Click(Sender: TObject);
    procedure ListBoxItem4Click(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure ListBoxItem8Click(Sender: TObject);
    procedure ListBoxItem5Click(Sender: TObject);
    //s�rio
  private
    { Private declarations }
    childForm : TfParent;
  public
    { Public declarations }
    procedure cadContaEdit(c : TConta);
    procedure saldoAltBtClick(Sender: TObject);
    procedure recadastroContas(contas : TArray<TContaRecad>);
    procedure statsMenu(active : boolean);
  end;

var
  fMain: TfMain;

implementation

{$R *.fmx}

uses uDB, uLoadBills, uInicial, uCadConta, uConfUser, uGraficoGastos,
  uAlterSaldo, uSaldoInit, uListaContas, uListaContasRecadastro,
  uComparativoGrafico;
{$R *.Windows.fmx MSWINDOWS}
{$R *.LgXhdpiPh.fmx ANDROID}

procedure TfMain.Button2Click(Sender: TObject);
begin
  MultiView1.HideMaster;
end;

procedure TfMain.cadContaEdit(c: TConta);
var conta : TConta;
begin
  conta := c;
  loadChild(TfCadastroConta.ClassName);
  (childForm as TfCadastroConta).loadConta(conta);
end;

procedure TfMain.statsMenu(active : boolean);
begin
  Multiview1.enabled := active;
  btLateralMenu.visible := active;
end;

procedure TfMain.FormActivate(Sender: TObject);
var notif : TPushData;
    x : integer;
begin
  notif := dDatabase.PushEvents1.StartupNotification;
  try
    if assigned(notif) then
      begin
        showmessage(notif.Message);
                //iOS
        {if(dDatabase.PushEvents1.StartupNotification.APS.Alert <> '') then
          begin
            for x := 0 to notif.Extras.Count -1 do
              Memo1.Lines.Add(notif.Extras[x].Key + '=' + notif.Extras[x].Value)
          end;}

        //Android
        {if(dDatabase.PushEvents1.StartupNotification.GCM.Message <> '') then
          begin
            for x := 0 to notif.Extras.Count -1 do
              Memo1.Lines.Add(notif.Extras[x].Key + '=' + notif.Extras[x].Value)
          end;}
      end;
  finally
    notif.DisposeOf;
  end;
end;

procedure TfMain.FormCreate(Sender: TObject);
begin
  dDatabase := TdDatabase.Create(Application);
  dDatabase.Connection.Connected := true;
  Sleep(1000);
  dDatabase.DoPushOn;
  loadChild(TfInicial.ClassName);
end;

procedure TfMain.ListBoxItem1Click(Sender: TObject);
begin
  loadChild(TfInicial.ClassName);
  MultiView1.HideMaster;
end;

procedure TfMain.ListBoxItem2Click(Sender: TObject);
begin
  loadChild(TfCadConf.ClassName);
  MultiView1.HideMaster;
end;

procedure TfMain.saldoAltBtClick(Sender: TObject);
begin
  loadChild(TfAlterSaldo.ClassName);
  MultiView1.HideMaster;
end;

procedure TfMain.saldoInitBtClick(Sender: TObject);
begin
  loadChild(TfSaldoInit.ClassName);
  MultiView1.HideMaster;
end;

procedure TfMain.ListBoxItem3Click(Sender: TObject);
begin
  loadChild(TfGraficoGastos.ClassName);
  MultiView1.HideMaster;
  TfGraficoGastos(childForm).ShowPieChart;
end;

procedure TfMain.ListBoxItem4Click(Sender: TObject);
begin
  loadChild(TfListaConta.ClassName);
  MultiView1.HideMaster;
end;

procedure TfMain.ListBoxItem5Click(Sender: TObject);
begin
  loadChild(TfComparativoGastos.ClassName);
  MultiView1.HideMaster;
end;

procedure TfMain.ListBoxItem6Click(Sender: TObject);
begin
  loadChild(TfAlterSaldo.ClassName);
  MultiView1.HideMaster;
end;

procedure TfMain.ListBoxItem8Click(Sender: TObject);
begin
  loadChild(TfContas.ClassName);
  MultiView1.HideMaster;
end;

procedure TfMain.loadChild(AClass : String);
var ObjClass: TFmxObjectClass;
    NewForm: TCustomForm;
begin

  if(childForm <> nil) then
    begin
      if(childForm.ClassName <> AClass)then
        begin
          //childForm.Destroy;
          FreeAndNil(childForm);
          loMain.Repaint;
          ObjClass := TFmxObjectClass(GetClass(AClass));
          if ObjClass <> nil then
            begin
              NewForm := ObjClass.Create(Self) as TCustomForm;
              childForm := TfParent(NewForm);
              childForm.loChild.Parent := loMain;
            end;
        end;
    end
  else
    begin
      ObjClass := TFmxObjectClass(GetClass(AClass));
      if ObjClass <> nil then
        begin
          NewForm := ObjClass.Create(Self) as TCustomForm;
          childForm := TfParent(NewForm);
          childForm.loChild.Parent := loMain;
        end;
    end;
end;

procedure TfMain.recadastroContas(contas: TArray<TContaRecad>);
var contasR : TArray<TContaRecad>;
begin
  contasR := contas;
  loadChild(TfLoadBills.ClassName);
  (childForm as TfLoadBills).activate(contasR);
end;

initialization
  RegisterFmxClasses([TfInicial, TfLoadBills, TfCadConf, TfCadastroConta, TfComparativoGastos,
                      TfGraficoGastos, TfAlterSaldo, TfSaldoInit, TfListaConta, TfContas]);
end.
