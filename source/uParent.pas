unit uParent;

interface

uses
  System.SysUtils, System.Types, System.UITypes, System.Classes, System.Variants,
  FMX.Types, FMX.Controls, FMX.Forms, FMX.Graphics, FMX.Dialogs, FMX.Layouts;

type
  TfParent = class(TForm)
    loChild: TLayout;
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  fParent: TfParent;

implementation

{$R *.fmx}

end.
