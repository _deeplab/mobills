unit uComparativoGrafico;

interface

uses
  System.SysUtils, System.Types, System.UITypes, System.Classes, System.Variants, 
  FMX.Types, FMX.Graphics, FMX.Controls, FMX.Forms, FMX.Dialogs, FMX.StdCtrls,
  uParent, FMX.Objects, FMX.Controls.Presentation, FMX.Layouts;

type
  TfComparativoGastos = class(TfParent)
    Layout1: TLayout;
    VertScrollBox1: TVertScrollBox;
    Panel1: TPanel;
    Panel4: TPanel;
    Panel6: TPanel;
    Panel2: TPanel;
    Panel3: TPanel;
    Label1: TLabel;
    pnl_1: TPanel;
    Rectangle3: TRectangle;
    Rectangle4: TRectangle;
    Panel8: TPanel;
    pnl_2: TPanel;
    Rectangle11: TRectangle;
    Rectangle12: TRectangle;
    Panel5: TPanel;
    pnl_3: TPanel;
    Rectangle7: TRectangle;
    Rectangle8: TRectangle;
    Panel13: TPanel;
    pnl_4: TPanel;
    Rectangle1: TRectangle;
    Rectangle2: TRectangle;
    Panel10: TPanel;
    pnl_5: TPanel;
    Rectangle5: TRectangle;
    Rectangle6: TRectangle;
    Panel15: TPanel;
    Panel17: TPanel;
    Panel19: TPanel;
    Panel21: TPanel;
    Panel23: TPanel;
    lbl_1: TLabel;
    lbl_2: TLabel;
    lbl_5: TLabel;
    lbl_3: TLabel;
    lbl_4: TLabel;
    Label7: TLabel;
    Label8: TLabel;
    Label9: TLabel;
    Label10: TLabel;
    Label11: TLabel;
    procedure FormCreate(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  fComparativoGastos: TfComparativoGastos;

implementation

{$R *.fmx}
{$R *.NmXhdpiPh.fmx ANDROID}
{$R *.LgXhdpiPh.fmx ANDROID}

procedure TfComparativoGastos.FormCreate(Sender: TObject);
var maxWidth : Single;
    barWidth : Single;
begin
  inherited;
  maxWidth := Round((Screen.Width - (Panel3.Width + Panel2.Width))-40);
  //as barrinhas do meio vai ser sempre 10, ent�o = 40;
  barWidth := Round(maxWidth / 5);
  pnl_1.Width := barWidth;
  pnl_2.Width := barWidth;
  pnl_3.Width := barWidth;
  pnl_4.Width := barWidth;
  pnl_5.Width := barWidth;

  lbl_1.Width := barWidth;
  lbl_2.Width := barWidth;
  lbl_3.Width := barWidth;
  lbl_4.Width := barWidth;
  lbl_5.Width := barWidth;
end;

end.
