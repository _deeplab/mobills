unit uGraficoGastos;

interface

uses
  System.SysUtils, System.Types, System.UITypes, System.Classes, System.Variants, 
  FMX.Types, FMX.Graphics, FMX.Controls, FMX.Forms, FMX.Dialogs, FMX.StdCtrls,
  uParent, FMX.Controls.Presentation, FMX.Layouts, FMX.Styles.Objects, FMX.Ani,
  FMX.Objects;

type
  TDataPie = record
    BillKind : String;
    BillPercentage : Single;
    BillColor : TAlphaColor;
  end;

  TfGraficoGastos = class(TfParent)
    Panel1: TPanel;
    Label1: TLabel;
    Rectangle1: TRectangle;
    Pie1: TPie;
    Circle1: TCircle;
    Text2: TText;
    FloatAnimation1: TFloatAnimation;
    StyleObject1: TStyleObject;
    Panel2: TPanel;
    VertScrollBox1: TVertScrollBox;
    procedure FloatAnimation1Process(Sender: TObject);
    procedure FormCreate(Sender: TObject);
  private
    { Private declarations }
    FValues : Array of TDataPie;
    FSum : Single;
  public
    { Public declarations }
    procedure ShowPieChart;
  end;

var
  fGraficoGastos: TfGraficoGastos;

implementation

{$R *.fmx}

uses uConstants, uDB;

procedure TfGraficoGastos.FloatAnimation1Process(Sender: TObject);
begin
  inherited;
  with FloatAnimation1 do
    begin
      Text2.Text := IntToStr(Round(((1/360) * (90+Pie1.StartAngle)) * 100))+'%';
      StyleObject1.Opacity := CurrentTime;
    end;
end;

procedure TfGraficoGastos.FormCreate(Sender: TObject);
var A : Integer;
    S : Single;
    val : TArray<TValorGrafico>;

const names : Array [0..5] of String = (' ','Casa','Mercado','Despesas Pessoais',
                                        'Carro','Outros');
      color : Array [0..5] of TAlphaColor = ($FFACC27F, $FF7CB4C9, $FFD59488,
                                             $FFDFCB7D, $FF72A892, $FFE6E6B3);
begin
  inherited;
  Fill.Kind := TBrushKind.Solid;
  Rectangle1.Stroke.Kind := TBrushKind.None;
  Circle1.Stroke.Kind := TBrushKind.None;
  Text2.Text := '';
  Pie1.Stroke.Kind := TBrushKind.None;
  Pie1.Fill.Assign(Self.Fill);
  Rectangle1.Padding.Rect := TRect.Create(5,5,5,5);
  Pie1.Align := TAlignLayout.Contents;

  //busca as contas e gera o array aqui
  val := dDataBase.getDadosGraficoGastos;
  SetLength(FValues, Length(val));
  for A := Low(val) to High(val) do
    begin
      FValues[A].BillKind       := names[val[A].TP_CONTA];
      FValues[A].BillPercentage := val[A].QT_CONTA;
      FValues[A].BillColor      := color[A];
      FSum := FSum + val[A].QT_CONTA;
    end;

  {SetLength(FValues, 2+Random(5));
  for A := Low(FValues) to High(FValues) do
    begin
      S := 10 + Random(1000);
      FValues[A].BillKind       := names[A];
      FValues[A].BillPercentage := S;
      FValues[A].BillColor      := color[A];
      FSum := FSum + S;
    end;}
end;

procedure TfGraficoGastos.ShowPieChart;
var K : TFmxObject;
    F,E,H,R,M : Single;
    A : Integer;
    P : TPie;
    S : String;
    L : TLayout;
    T : TText;
begin
  //clear view
  Rectangle1.RemoveObject(Pie1);
  Rectangle1.RemoveObject(StyleObject1);
  while Rectangle1.ChildrenCount > 0 do
    for K in Rectangle1.Children do
      begin
        Rectangle1.RemoveObject(K);
        K.Free;
      end;
  StyleObject1 := TStyleObject.Create(nil);
  F := 0.2;
  //create view
  E := -90;
  for A := Low(FValues) to High(FValues) do
    begin
      P := TPie.Create(nil);
      P.StartAngle := E;
      R := (FValues[A].BillPercentage / FSum) * 360;
      P.EndAngle := E + R;
      M := E + R * 0.5;
      E := P.EndAngle;
      P.Fill.Color := FValues[A].BillColor;
      P.Stroke.Kind := TBrushKind.None;
      Rectangle1.AddObject(P);
      P.Align := TAlignLayout.Client;
      L := TLayout.Create(nil);
      L.RotationCenter.Point := TPointF.Zero;
      StyleObject1.AddObject(L);
      with Rectangle1.LocalRect.CenterPoint do L.SetBounds(X,Y,0,0);
      L.RotationAngle := M;
      T := TText(Text2.Clone(nil));
      T.Align := TAlignLayout.None;
      T.Width := 80;
      T.Text :=  IntToStr(Round((FValues[A].BillPercentage / FSum) * 100))+'%'+LineFeed+FValues[A].BillKind;
      L.Width:=(Rectangle1.LocalRect.BottomRight.Length * F) + (T.LocalRect.BottomRight.Length * 0.5);
      StyleObject1.AddObject(T);
      T.Position.Point := StyleObject1.AbsoluteToLocal(L.AbsoluteRect.BottomRight)-T.LocalRect.CenterPoint;
    end;

  Pie1.StartAngle := -90;
  Pie1.EndAngle := 270;
  Rectangle1.AddObject(Pie1);
  Rectangle1.AddObject(StyleObject1);
  FloatAnimation1.Start;
end;

end.
