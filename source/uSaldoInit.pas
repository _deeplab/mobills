unit uSaldoInit;

interface

uses
  System.SysUtils, System.Types, System.UITypes, System.Classes, System.Variants, 
  FMX.Types, FMX.Graphics, FMX.Controls, FMX.Forms, FMX.Dialogs, FMX.StdCtrls,
  uParent, FMX.Layouts, FMX.Objects, FMX.Edit, FMX.Controls.Presentation, FMX.DialogService;

type
  TfSaldoInit = class(TfParent)
    Panel1: TPanel;
    Panel2: TPanel;
    Panel3: TPanel;
    Panel5: TPanel;
    Label1: TLabel;
    Panel4: TPanel;
    Edit2: TEdit;
    Panel9: TPanel;
    Panel11: TPanel;
    Panel12: TPanel;
    Rectangle2: TRectangle;
    Label6: TLabel;
    VertScrollBox1: TVertScrollBox;
    procedure Label6Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  fSaldoInit: TfSaldoInit;

implementation

{$R *.fmx}

uses uDB, uInicial, uMain;
{$R *.LgXhdpiPh.fmx ANDROID}
{$R *.NmXhdpiPh.fmx ANDROID}

procedure TfSaldoInit.Label6Click(Sender: TObject);
var saldoAnt : Double;
    res : boolean;
begin
  inherited;
  if(Edit2.Text = '') then
    begin
      TDialogService.MessageDialog('O campo deve ter algum valor!',
        TMsgDlgType.mtwarning, [TMsgDlgBtn.mbOK],
        TMsgDlgBtn.mbOK, 0,
        procedure(const AResult: TModalResult)
          begin
            if AResult = mrOK then
              //DO-Nothing
          end);
      exit;
    end;

  saldoAnt := dDatabase.getSaldo;
  if(saldoAnt <> 0) then
    begin
      TDialogService.MessageDialog('Deseja adicionar o saldo do m�s anterior?',
      TMsgDlgType.mtConfirmation, [TMsgDlgBtn.mbYes,TMsgDlgBtn.mbNo],
      TMsgDlgBtn.mbNo, 0,
      procedure(const AResult: TModalResult)
        begin
        if(AResult = mrYes) then
          res := dDatabase.setSaldoInicial(StrToFloat(Edit2.Text) + saldoAnt)
        else
          res := dDatabase.setSaldoInicial(StrToFloat(Edit2.Text));
        end);
    end
  else
    res := dDatabase.setSaldoInicial(StrToFloat(Edit2.Text));

  if(res) then
    begin
      TDialogService.MessageDialog('Saldo do m�s cadastrado com sucesso!',
        TMsgDlgType.mtInformation, [TMsgDlgBtn.mbOK],
        TMsgDlgBtn.mbOK, 0,
        procedure(const AResult: TModalResult)
          begin
            if AResult = mrOK then
              fMain.loadChild(TfInicial.ClassName)
          end);
    end
  else
    TDialogService.MessageDialog('Ocorreu um problema ao cadastrar saldo do m�s',
        TMsgDlgType.mtError, [TMsgDlgBtn.mbOK],
        TMsgDlgBtn.mbOK, 0,
        procedure(const AResult: TModalResult)
          begin
            if AResult = mrOK then
              //Nothing
          end);
end;

end.
