unit uMainServ;

interface

uses
  System.SysUtils,
  System.Classes,
  System.Android.Service,
  AndroidApi.JNI.GraphicsContentViewText,
  Androidapi.JNI.Os, System.DateUtils,
  AndroidApi.JNI.App, Data.DB,
  Data.SqlExpr, System.IOUtils,
  System.Net.HTTPClient, JSON, Data.DbxSqlite,
  Data.FMTBcd, System.Threading, System.Net.URLClient,
  System.Net.HttpClientComponent;

type
  TContas = record
    ID : Integer;
    DT_VENCIMENTO : TDate;
    FL_PAGO : boolean;
  end;

  TGasto = record
    ID : Integer;
    NR_MES : Integer;
    VL_GASTO : Double;
  end;

  TDM = class(TAndroidService)
    SQLConnection1: TSQLConnection;
    SQLQuery1: TSQLQuery;
    function AndroidServiceStartCommand(const Sender: TObject;
      const Intent: JIntent; Flags, StartId: Integer): Integer;
    procedure SQLConnection1BeforeConnect(Sender: TObject);
  private
    { Private declarations }
    procedure notifyBills();
    procedure deleteContas();
    procedure deleteSaldoMensal();
    procedure notifyRecadastro();
  public
    { Public declarations }
  end;

var
  DM: TDM;

implementation

{%CLASSGROUP 'FMX.Controls.TControl'}

{$R *.dfm}

function TDM.AndroidServiceStartCommand(const Sender: TObject;
  const Intent: JIntent; Flags, StartId: Integer): Integer;
var ret : boolean;
begin
  Result := TJService.JavaClass.START_STICKY;
  TTask.run(procedure
    begin
      while true do
        begin
          SQLConnection1.Connected := true;
          notifyBills;
          deleteContas;
          deleteSaldoMensal();
          SQLConnection1.Connected := false;
          Sleep(5000);
        end;
    end
  );
end;

procedure TDM.deleteContas;
var contas : TArray<TContas>;
    conta : TContas;
    id_delete : String;
    I: Integer;
    idx : Integer;
begin
  idx := 0;
  SetLength(contas,0);
  SQLQuery1.Close;
  SQLQuery1.SQL.Text := 'SELECT rowid, * FROM CONTA';
  SQLQuery1.Open;
  while not SQLQuery1.Eof do
    begin
      conta.ID := SQLQuery1.FieldByName('rowid').AsInteger;
      conta.DT_VENCIMENTO := StrToDate(SQLQuery1.FieldByName('DT_VENCIMENTO').AsString);
      if SQLQuery1.FieldByName('FL_PAGO').AsInteger = 0 then
        conta.FL_PAGO := false
      else
        conta.FL_PAGO := true;

      SetLength(contas,Length(contas)+1);
      contas[idx] := conta;
      idx := idx + 1;
      SQLQuery1.Next;
    end;

  if Length(contas) > 0 then
    begin
      for I := 0 to Length(contas)-1 do
        begin
          if id_delete <> '' then
            id_delete := id_delete + ', ';

          if(MonthOf(contas[I].DT_VENCIMENTO) <= MonthOf(IncMonth(Now,(2*-1)))) and (contas[I].FL_PAGO = true)then
            id_delete := id_delete + IntToStr(contas[I].ID);
        end;

      if id_delete <> '' then
        begin
          SQLQuery1.Close;
          SQLQuery1.SQL.Text := 'DELETE FROM CONTA WHERE rowid IN('+id_delete+')';
          SQLQuery1.ExecSQL;
          SQLConnection1.Commit;
        end;
    end;
end;

procedure TDM.deleteSaldoMensal;
var gasto : TGasto;
    gastos : TArray<TGasto>;
    ids :  String;
    idx : Integer;
    I : Integer;
begin
  ids := '';
  idx := 0;
  SetLength(gastos,0);
  SQLQuery1.Close;
  SQLQuery1.SQL.Text := 'SELECT rowid, * FROM GASTO_MES';
  SQLQuery1.Open;
  while not SQLQuery1.Eof do
    begin
      gasto.ID := SQLQuery1.FieldByName('rowid').AsInteger;
      gasto.NR_MES := SQLQuery1.FieldByName('NR_MES').AsInteger;
      gasto.VL_GASTO := SQLQuery1.FieldByName('VL_CONTAS_MES').AsFloat;
      SetLength(gastos,Length(gastos)+1);
      gastos[idx] := gasto;
      idx := idx + 1;
      SQLQuery1.Next;
    end;
  if Length(gastos) > 0 then
    begin
      for I := 0 to Length(gastos)-1 do
        begin
          if ids <> '' then
            ids := ids + ', ';

          if(gastos[I].NR_MES < MonthOf(IncMonth(Now,(5*-1))))then
            ids := ids + IntToStr(gastos[I].ID);
        end;

      if ids <> '' then
        begin
          SQLQuery1.Close;
          SQLQuery1.SQL.Text := 'DELETE FROM GASTO_MES WHERE rowid IN('+ids+')';
          SQLQuery1.ExecSQL;
          SQLConnection1.Commit;
        end;
    end;
end;

procedure TDM.notifyBills;
var
  client : THTTPClient;
  json : TJSONObject;
  jsondata : TJSONObject;
  jsonnotif : TJSONObject;
  registered : TJSONArray;
  data : TStringStream;
  response : TStringStream;
  token : String;
  conta : TArray<TContas>;
  idx : Integer;
  contaI : TContas;
  dias_aviso : Integer;
const
  proj_code : String = '943284183875';
  api : String = 'AIzaSyAKgahJv6nGxwY_i6yE4cO0C6X3LGyQnpM';
  url : String = 'https://fcm.googleapis.com/fcm/send';
begin
  try
    SQLQuery1.Close;
    SQLQuery1.SQL.Text := 'SELECT DT_LAST_NOTIFY_BILL FROM USER';
    SQLQuery1.Open;

    if not StrToDate(SQLQuery1.FieldByName('DT_LAST_NOTIFY_BILL').AsString) < StrToDate(DateToStr(Now)) then
      begin
        SQLQuery1.Close;
        SQLQuery1.SQL.Text := 'SELECT DS_TOKEN FROM USER';
        SQLQuery1.Open;
        if not SQLQuery1.IsEmpty then
          begin
            token := SQLQuery1.FieldByName('DS_TOKEN').AsString;

            SQLQuery1.Close;
            SQLQuery1.SQL.Text := 'SELECT * FROM USER';
            SQLQuery1.Open;
            dias_aviso := StrToIntDef(SQLQuery1.FieldByName('NR_DIAS_AVISO_VENCIMENTO').AsString,0);
            dias_aviso := dias_aviso * -1;
            idx := 0;
            SetLength(conta,0);
            SQLQuery1.Close;
            SQLQuery1.SQL.Text := 'SELECT rowid, * FROM CONTA';
            SQLQuery1.Open;
            while not SQLQuery1.Eof do
              begin
                contaI.ID := SQLQuery1.FieldByName('rowid').AsInteger;
                contaI.DT_VENCIMENTO := StrToDate(SQLQuery1.FieldByName('DT_VENCIMENTO').AsString);
                if SQLQuery1.FieldByName('FL_PAGO').AsInteger = 0 then
                  contaI.FL_PAGO := false
                else
                  contaI.FL_PAGO := true;

                if((contaI.FL_PAGO = false) and ((IncDay(contaI.DT_VENCIMENTO,(dias_aviso)) <= Now)) and (MonthOf(contaI.DT_VENCIMENTO) = MonthOf(Now)))then
                  begin
                    SetLength(conta,Length(conta)+1);
                    conta[idx] := contaI;
                    idx := idx + 1;
                  end;
                SQLQuery1.Next;
              end;

            if Length(conta) > 0 then
              begin
                registered := TJSONArray.Create;
                registered.Add(token);

                jsondata := TJSONObject.Create;
                jsondata.AddPair('id',proj_code);
                jsondata.AddPair('message','Voc� tem contas n�o pagas a vencer, verifique!');
                jsondata.AddPair('actionIndex','0');

                jsonnotif := TJSONObject.Create;
                jsonnotif.AddPair('title','Mobills');
                jsonnotif.AddPair('body','Voc� tem contas n�o pagas a vencer, verifique!');


                json := TJSONObject.Create;
                json.AddPair('registration_ids',registered);
                json.AddPair('data',jsondata);
                json.AddPair('notification',jsonnotif);

                client := THTTPClient.Create;
                client.ContentType := 'application/json';
                client.CustomHeaders['Authorization'] := 'key='+api;

                data := TStringStream.Create(json.ToString);
                data.Position := 0;

                response := TStringStream.Create;

                client.Post(url,data,response);
                response.Position := 0;

                SQLQuery1.Close;
                SQLQuery1.SQL.Text := 'UPDATE USER SET DT_LAST_NOTIFY_BILL = :DT';
                SQLQuery1.Params.ParamByName('DT').AsString := DateToStr(Now);
                SQLQuery1.ExecSQL();
                SQLConnection1.Commit;
              end;
          end;
      end;
  except

  end;
end;

procedure TDM.notifyRecadastro;
begin
  //TODO
end;

procedure TDM.SQLConnection1BeforeConnect(Sender: TObject);
begin
  {$IF DEFINED(iOS) or DEFINED(ANDROID)}
    SQLConnection1.Params.Values['ColumnMetadataSupported'] := 'False';
    SQLConnection1.Params.Values['Database'] := TPath.Combine(TPath.GetDocumentsPath, 'mobills.s3db');
  {$ENDIF}
end;

end.
